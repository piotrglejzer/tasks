import styled from '@emotion/styled';

const NavbarEm = styled.div`
  width: 100%;
  height: 73px;
  background-color: #494e61;
`;

const NavbarWrapper = styled.div`
  display: flex;
  height: 100%;
  justify-content: flex-start;
  align-items: center;
  margin-left: 17px;
  position: relative;
  &::after {
    content: '';
    position: absolute;
    left: 194px;
    bottom: 18px;
    height: 35px;
    width: 2px;
    background-color: #757b8f;
  }
`;
const NavbarText = styled.div`
  color: #ffffff;
  font-family: Montserrat;
  font-size: 14px;
  font-weight: bold;
  text-align: left;
`;
const NavbarCircle = styled.div`
  border: 2px solid #7d82f7;
  border-radius: 50px/50px;
  width: 13px;
  height: 13px;
  margin-right: 11px;
  margin-top: 2px;
  margin-left: 7.3px;
`;

export { NavbarEm, NavbarWrapper, NavbarText, NavbarCircle };
