import React from 'react';
import { NavbarCircle, NavbarEm, NavbarText, NavbarWrapper } from './emotion';

export default function Navbar() {
  return (
    <NavbarEm>
      <NavbarWrapper>
        <NavbarCircle />
        <NavbarText>Recruitment task</NavbarText>
      </NavbarWrapper>
    </NavbarEm>
  );
}
