import React from 'react';
import { Ring, RingDiv } from './emotion';

export default function Loader() {
  return (
    <Ring className="lds-ring">
      {Array(5)
        .fill()
        .map((_, i) => (
          <RingDiv key={`ring-${i}`} />
        ))}
    </Ring>
  );
}
