import styled from '@emotion/styled';
import { keyframes } from '@emotion/react';

const pulsate = keyframes`
    0% {transform: scale(0.1, 0.1); opacity: 0.0;}
    50% {opacity: 1.0;}
    100% {transform: scale(1.2, 1.2); opacity: 0.0;}
`;

const ListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ListBox = styled.div`
  box-sizing: border-box;
  height: 100%;
  margin-bottom: 30px;
  width: 1104px;
  border: 2px solid #ededf0;
  background-color: #ffffff;
  position: relative;
  margin-top: 92px;
  margin-right: 81px;
  padding-top: 34px;
  padding-left: 42px;
`;

const ListServrNumberWrapper = styled.div`
  position: absolute;
  top: -70px;
  display: flex;
  flex-direction: column;
  left: 0;
`;

const ListServerText = styled.span`
  height: 15px;
  color: #494e61;
  font-family: 'Open Sans';
  font-size: 21px;
  font-weight: 500;
  line-height: 44px;
  padding-left: 1px;
`;

const ListNumberText = styled(ListServerText)`
  height: 12px;
  font-size: 15px;
  margin-top: 10px;
`;

const ListInputWrapper = styled.div`
  position: absolute;
  top: -52px;
  right: -1px;
`;

const ListHeaderNameStatus = styled.div`
  display: flex;
  color: #9ca7d3;
  font-family: 'Open Sans';
  font-size: 14px;
  font-weight: 400;
  text-transform: uppercase;
  position: relative;
  padding-bottom: 21px;

  &::before {
    content: '';
    border-bottom: 2px solid #ededf0;
    position: absolute;
    width: 104%;
    left: -42px;
    bottom: 0;
  }
`;

const ListHeaderName = styled.div`
  padding-right: 247px;
`;

const ListElementWrapper = styled.div`
  display: flex;
  color: #494e61;
  font-family: 'Open Sans';
  font-size: 13px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: 59px;
  position: relative;

  &::after {
    content: '';
    border-bottom: 2px solid #ededf0;
    position: absolute;
    width: 104%;
    left: -42px;
    bottom: 0;
  }
`;

const ListElementName = styled.div`
  width: ${({ empty }) => (empty ? '100%' : '212px')};
  padding-right: 75px;
`;

const ListElementStatusOn = styled.div`
  color: #47b1b8;
  position: relative;
  padding-left: 7px;
  letter-spacing: -1px;

  &::before {
    content: '';
    position: absolute;
    height: 7px;
    width: 7px;
    border-radius: 100%;
    background-color: #25cad5;
    left: -8px;
    top: calc(50% - 3px);
    z-index: 10;
  }

  &::after {
    content: '';
    position: absolute;
    border: 2px solid #25cad5;
    height: 7px;
    width: 7px;
    left: -10px;
    top: calc(50% - 5px);
    animation: ${pulsate} 1s ease-out;
    animation-iteration-count: infinite;
    opacity: 0;
    border-radius: 100%;
  }
`;
const ListElementStatusOff = styled.div`
  color: #494e61;
  position: relative;
  padding-left: 7px;
  letter-spacing: -1px;
  height: 7px;
  width: 6px;

  &::before,
  &:after {
    content: '';
    position: absolute;
    width: 100%;
    height: 2px;
    background-color: #ec5684;
    left: -12px;
    top: 29px;
    z-index: 10;
    transform: rotate(45deg);
  }

  &::before {
    transform: rotate(45deg);
  }

  &::after {
    transform: rotate(-45deg);
  }
`;

const ListElementDots = styled.div`
  position: absolute;
  right: 20px;
  letter-spacing: 0.5px;
  font-size: 24px;
  top: calc(50% - 8px);
  transform: translate(-50%, -50%);
  user-select: none;
  color: #b1b1b4;
  cursor: pointer;
`;

const ListElementStautusReb = styled.div`
  margin-left: -10px;
  color: #9ca6d5;
  font-weight: 500;
  font-size: 12px;
`;

const ListDropdown = styled.div`
  background-color: #ffffff;
  width: 137px;
  position: absolute;
  right: -3px;
  box-shadow: 1px 1px 7px rgba(0, 0, 0, 0.3);
  z-index: 999;
`;

const ListDropdownElement = styled.div`
  width: 100%;
  z-index: 999;
  font-family: 'Open Sans';
  font-size: 13px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: 50px;
  height: 55px;
  display: flex;
  justify-content: center;
  transition: all 0.3s ease;
  cursor: pointer;
  &:hover {
    background-color: #f2f3f6;
  }
`;

export {
  ListWrapper,
  ListBox,
  ListServrNumberWrapper,
  ListServerText,
  ListNumberText,
  ListInputWrapper,
  ListHeaderNameStatus,
  ListHeaderName,
  ListElementWrapper,
  ListElementName,
  ListElementStatusOn,
  ListElementDots,
  ListElementStatusOff,
  ListElementStautusReb,
  ListDropdown,
  ListDropdownElement,
};
