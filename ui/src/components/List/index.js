import React, { useCallback, useEffect, useState, memo } from 'react';
import Input from '../Input';
import {
  ListBox,
  ListDropdown,
  ListDropdownElement,
  ListElementDots,
  ListElementName,
  ListElementStatusOff,
  ListElementStatusOn,
  ListElementStautusReb,
  ListElementWrapper,
  ListHeaderName,
  ListHeaderNameStatus,
  ListInputWrapper,
  ListNumberText,
  ListServerText,
  ListServrNumberWrapper,
  ListWrapper,
} from './emotion';

import Loader from '../Loader';

const errorMessage = (message) => `There is a problem with your app with error type:${message}`;

const List = memo(() => {
  const [allData, setAllData] = useState([]);
  const [mockedData, setMockedData] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [isLoading, setIsLoading] = useState(true);

  const fetchData = async (link, method = 'GET') => {
    try {
      const response = await fetch(`http://localhost:4454/${link}`, { method });
      const json = await response.json();

      if (response.status === 200) setIsLoading(false);

      return json;
    } catch (error) {
      console.log(errorMessage(error));
      throw new Error(errorMessage(error));
    }
  };

  const handleClickOnOff = useCallback(
    ({ target }, id) => {
      if (Number(target.id) === id) {
        const data = allData.map((e) => {
          if (e.id === id) {
            return { ...e, isDropdown: true };
          } else {
            return { ...e, isDropdown: false };
          }
        });
        setAllData(data);
      }
    },
    [allData]
  );

  useEffect(() => {
    const data = mockedData
      .filter(
        ({ name, status, id }) =>
          name.toLowerCase().includes(searchTerm) ||
          status.toLowerCase().includes(searchTerm) ||
          id.toString() === searchTerm
      )
      .map((e) => {
        return { ...e, isDropdown: false };
      });

    setAllData(data);
  }, [searchTerm, mockedData]);

  useEffect(() => {
    async function fetchAllData() {
      const json = await fetchData('servers');
      const mapped = json.map((e) => {
        return { ...e, isDropdown: false };
      });
      setAllData(mapped);
      setMockedData(mapped);
    }

    fetchAllData();
  }, []);

  const handleClickBody = useCallback(
    ({ target: { classList } }) => {
      if (
        !classList.contains('list-dropdown') &&
        !classList.contains('list-dropdown-element') &&
        !classList.contains('list-dots')
      ) {
        const data = allData.map((e) => {
          return { ...e, isDropdown: false };
        });

        setAllData(data);
      }
    },
    [allData]
  );

  useEffect(() => {
    window.addEventListener('click', handleClickBody);
    return () => {
      window.removeEventListener('click', handleClickBody);
    };
  }, [handleClickBody]);

  const handleClickByType = useCallback(
    async (putId = '', type = '') => {
      const json = await fetchData(`servers/${putId}/${type}`, 'PUT');

      const data = allData.filter(({ id }) => id !== json.id);
      const sorted = [...data, json].sort((a, b) => a.id - b.id);
      setAllData([...sorted]);
    },
    [allData]
  );

  const handleClickReb = useCallback(
    async (putId) => {
      handleClickByType(putId, 'reboot');

      await fetchData(`servers/${putId}`);

      setMockedData(await fetchData('servers'));

      const timer = setInterval(async () => {
        const jsonAll = await fetchData('servers');
        const { status } = await fetchData(`servers/${putId}`);

        setMockedData(jsonAll);

        if (status === 'ONLINE') {
          if (allData.length !== jsonAll.length) {
            const filterd = jsonAll.filter((item) => allData.find(({ id }) => item.id === id));

            setAllData(filterd);
          } else {
            setAllData([...jsonAll]);
          }

          clearInterval(timer);
        }
      }, 1000);
    },
    [allData, handleClickByType]
  );

  const statmentForStatus = (status, id, isDropdown) => {
    if (status === 'OFFLINE') {
      return (
        <>
          <ListElementStatusOff>{status}</ListElementStatusOff>
          <ListElementDots id={id} onClick={(e) => handleClickOnOff(e, id)} className="list-dots" role="button">
            ...
          </ListElementDots>
          {isDropdown && (
            <ListDropdown className="list-dropdown">
              <ListDropdownElement
                className="list-dropdown-element"
                onClick={() => handleClickByType(id, 'on')}
                role="button"
              >
                Turn on
              </ListDropdownElement>
            </ListDropdown>
          )}
        </>
      );
    } else if (status === 'ONLINE') {
      return (
        <>
          <ListElementStatusOn>{status}</ListElementStatusOn>
          <ListElementDots id={id} onClick={(e) => handleClickOnOff(e, id)} className="list-dots" role="button">
            ...
          </ListElementDots>
          {isDropdown && (
            <ListDropdown className="list-dropdown">
              <ListDropdownElement
                className="list-dropdown-element"
                onClick={() => handleClickByType(id, 'off')}
                role="button"
              >
                Turn off
              </ListDropdownElement>
              <ListDropdownElement className="list-dropdown-element" onClick={() => handleClickReb(id)} role="button">
                Reboot
              </ListDropdownElement>
            </ListDropdown>
          )}
        </>
      );
    } else {
      return (
        <>
          <ListElementStautusReb>{status}...</ListElementStautusReb>
          <ListElementDots role="button">...</ListElementDots>
        </>
      );
    }
  };

  const handleInputChange = ({ target: { value } }) => setSearchTerm(value.toLowerCase());

  return (
    <ListWrapper>
      <ListBox>
        <ListServrNumberWrapper>
          <ListServerText>Servers</ListServerText>
          <ListNumberText>Number of elements: {allData.length}</ListNumberText>
        </ListServrNumberWrapper>
        <ListInputWrapper>
          <Input value={searchTerm} onChange={handleInputChange} id="input-search" name="search-input" />
        </ListInputWrapper>
        <ListHeaderNameStatus>
          <ListHeaderName>Name</ListHeaderName>
          <div>Status</div>
        </ListHeaderNameStatus>

        <>
          {isLoading ? (
            <Loader />
          ) : allData.length > 0 ? (
            allData.map(({ status, name, id, isDropdown }, i) => {
              return (
                <ListElementWrapper key={name + i}>
                  <ListElementName>{name}</ListElementName>

                  {statmentForStatus(status, id, isDropdown)}
                </ListElementWrapper>
              );
            })
          ) : (
            <ListElementWrapper>
              <ListElementName empty>No data found in the search parameters</ListElementName>
            </ListElementWrapper>
          )}
        </>
      </ListBox>
    </ListWrapper>
  );
});

export default List;
