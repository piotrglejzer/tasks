import styled from '@emotion/styled';

const InputWrapper = styled.div`
  position: relative;
  transition: 0.3s ease all;
  cursor: ${({ disabled }) => disabled && 'not-allowed'};
  opacity: ${({ disabled }) => disabled && 0.5};

  &::before {
    box-sizing: border-box;
    content: '';
    position: absolute;
    height: 14px;
    width: 14px;
    border: 2px solid #a9aec1;
    background-color: transparent;
    border-radius: 100%;
    left: 14px;
    top: 10px;
    z-index: 10;
  }

  &::after {
    box-sizing: border-box;
    content: '';
    position: absolute;
    height: 6px;
    width: 6px;
    border-bottom: 2px solid #a9aec1;
    transform: rotate(48deg);
    left: 25px;
    top: 19px;
    z-index: 10;
  }
`;

const InputElement = styled.input`
  box-sizing: border-box;
  height: 38px;
  width: 263px;
  border: 2px solid #d4d7e1;
  background-color: #f2f3f6;
  border-radius: 25px;
  padding-left: 48px;
  font-family: 'Open Sans';
  font-size: 14px;
  font-weight: 600;
  line-height: 44px;
  outline: none;
  color: #a9aec1;

  &::placeholder {
    opacity: 0.5;
  }
`;

export { InputWrapper, InputElement };
