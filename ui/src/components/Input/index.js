import React from 'react';
import { InputWrapper, InputElement } from './emotion';

export default function Input({ disabled, ...props }) {
  return (
    <InputWrapper disabled={disabled}>
      <InputElement type="search" placeholder="Search" {...props} disabled={disabled} />
    </InputWrapper>
  );
}
